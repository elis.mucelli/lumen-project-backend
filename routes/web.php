<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->post("/login", "UsersController@login");
$router->get("/user/", "UsersController@show");
$router->get("/logout", "UsersController@logout");
$router->get("/check", "UsersController@checkToken");
