<?php

namespace App\Http\Controllers;

use App\User;
use App\Token;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class UsersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware("auth", ["only" => ["show", "logout", "checkToken"]]);
    }

    public function login(Request $request)
    {
        $rules = [

            "email" => "required",
            "password" => "required"
        ];

        $this->validate($request, $rules);

        $user = User::where(["email" => $request->email, "password" => $request->password])->firstOrFail();

        $token = new Token();
        $token->token = base64_encode(str_random(40));
        $token->status = 1;
        $token->user_id = $user->id;
        $token->save();

        return response()->json(["user" => $user, "token" => $token->token]);

    }

    public function checkToken()
    {
        return response()->json(["message" => "OK"]);
    }

    public function show(Request $request)
    {
       return response()->json(Auth::user());
    }

    public function logout(Request $request)
    {
        $token = Token::where(["token" => $request->bearerToken(), "status" => 1])->first();

        if($token)
        {
            $token->status = 0;
            $token->update();
            return response()->json(["message" => "Logged out"]);
        }
    }

}
