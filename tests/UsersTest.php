<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class UsersTest extends TestCase
{

    public $token = "WTU4WVV2Tng0Q1hsaU44aEdsdG9RUE1QZk9jZ0p3UERlNnk0RzZWdg==";

    public function testShowUser()
    {
        $header = [ 'Authorization' => "Bearer {$this->token}"];
        $this->get("/user", $header);
        $this->seeStatusCode(200);
        $this->seeJsonStructure(["id", "first_name", "last_name", "email", "created_at","updated_at"]);
    }

    public function testLogin()
    {
        $this->post("/login", ["email" => "yundt.jammie@gmail.com", "password" => "!<2FKcuGgY~7"]);
        $this->seeStatusCode(200);
        $this->seeJsonStructure(["user" => ["id", "first_name", "last_name", "email", "created_at","updated_at"], "token"]);
    }

    public function testLogout()
    {
        $header = [ 'Authorization' => "Bearer {$this->token}"];
        $this->get("/logout", $header);
        $this->seeStatusCode(200);
        $this->seeJsonStructure(["message"]);

        /**
         * Set token active again
         */
        $token = \App\Token::where(["token" => $this->token])->first();
        $token->status = 1;
        $token->update();
    }

    public function actionTestAuthorization()
    {
        $this->get("/user");
        $this->seeStatusCode(401);
    }
}
